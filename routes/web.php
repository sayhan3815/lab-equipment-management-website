<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('FrontPage/FrontPage');
});



//for users

//Route::resource('Users','usersController');

Route::get('Users/','usersController@index');

Route::view('Users/addStudent/','Users/addStudent');

Route::post('Users/addStudent','UsersController@store');

Route::view('Users/addTeacher/','Users/addTeacher');

Route::post('Users/addTeacher','usersController@addTeacher');

Route::get('Users/allUsers','usersController@showUsers');

Route::view('Users/deleteUser/','Users.deleteUser');

Route::post('Users/deleteUser','usersController@destroy');

Route::get('Users/dept_wise','usersController@dept_wise_teacher');



//for Items

Route::get('/Items','itemController@index');

Route::get('Items/create','itemController@create');

Route::get('Items/delete','itemController@delete');

Route::get('Items/addtoInventory','itemController@inventoryAdd');

Route::get('Items/orderItem','itemController@orderItem');



//for Inventory

Route::get('/Inventory','inventoryController@index');

Route::get('/Inventory/addInventory','inventoryController@create');

Route::get('/Inventory/deleteInventory','inventoryController@delete');

Route::get('/Inventory/addItem','inventoryController@addModify');

Route::get('/Inventory/deleteItem','inventoryController@delModify');



//for Orders

Route::get('/Orders','orderController@create');

Route::get('order/all','orderController@index');

Route::get('order/make','orderController@make');

