<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        return view('Users.Users');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('Users.addUser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //

        //echo "<h1>Hello World</h1>";
        $id=request('id');
        $studentId=request('studentId');
        $department=request('department');
        $section=request('section');
        $batch=request('batch');
        $course_id=request('course');

        DB::insert("INSERT INTO users(id, user_type) VALUES(?, ?)",[$id,'Student']);
        DB::insert('INSERT INTO students (user_id, batch, department, section, id, course_id)
        VALUES (?, ?, ?, ?, ?, ?)',[$id,$batch, $department, $section,$studentId,$course_id]);


        //return redirect('Users/addStudent');
       


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        
        // echo 'hi'.'<br>';
        return view('Users.deleteUser');
    }

    //user-defined functions->
    public function addStudent()
    {
        //

        return view('Users.addStudent');
    }
    public function addTeacher()
    {
        //

        $id=request('id');
        $user_id=request('teacherId');
        $name=request('name');
        $department=request('department');
        $designation=request('designation');

        DB::insert("INSERT INTO users(id, user_type) VALUES(?, ?)",[$id,'Teacher']);
        DB::insert('INSERT INTO teachers (id,user_id, t_name, department, designation);

        VALUES (?,?, ?, ?, ?)',[$user_id,$id,$name, $department, $designation]);


    }

    public function showUsers()
    {
        //

        $students = DB::select('select s.*, u.user_type from Users as u natural join Students as s;');
        $teachers = DB::select('select t.*, u.user_type from Users as u natural join Teachers as t;');
        return view('Users.showUsers',['students'=>$students,'teachers'=>$teachers]);

    }

    public function dept_wise_teacher()
    {
        //

        $dept_count = DB::select("select t.department, count(*) TOTAL from Teachers as t natural join Users as u GROUP BY t.department ORDER BY TOTAL desc;");
        return view('Users.dept_wise_teacher',['teachers'=>$dept_count]);
        
    }

}
