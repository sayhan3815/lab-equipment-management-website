<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Remove User</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
<style>

#formOne {
    margin: auto;
    margin-top: 200px;
    border:2px solid darkcyan;
    width: 300px;
    padding: 10px 5px 10px 5px;
}

span {
    font-size: 20px;
}

#submit {
    margin-left: 230px;
    display: block;
}

#goback {
    margin-left: 57%;
    background-color: cyan;
    height: cover;
    padding: 4px;
    margin-top: 500px;
    color: #212;
}

</style>

</head>
<body>
<?php

if($_SERVER['REQUEST_METHOD']=='POST')
{
    $servername="localhost";
    $username="root";
    $password="";
    $dbname="lab_equipment_management";

    $conn=new mysqli($servername,$username,$password,$dbname);
    #Check Connection ->
    if ($conn->connect_error)
    {
        die("Connection failed: " . $conn->connect_error);
    }

    $user_id=$_POST['user_id'];

    $sql = $conn->prepare("DELETE FROM users WHERE user_id = ?");

    $sql->bind_param("s",$user_id);

    if($sql->execute()===TRUE)
    {
        echo 'Record removed successfully'.'<br>';
    } else {
        echo '<h3>Error occured when removing record</h3>';
    }

}




?>
    <form id='formOne' action="deleteUser" method="post">
        <span>USER ID:</span>
        <input type="text" name="user_id"><br><br>
        <input id='submit' type="submit" name="submit">
        @csrf
    </form>
    <a href="/Users" id='goback'>Go Back!</a>
</body>
</html>