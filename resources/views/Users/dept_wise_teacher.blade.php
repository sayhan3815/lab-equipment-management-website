<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Teacher List</title>
<style>

table {
    margin: auto;
    border: 2px solid green;
    text-align: center;
    color: blue;
    margin-bottom: 5px;
}

.student {
    width: 50%;
}

.table {
    width: 60%;
}

.tableHeader {
    text-align: center;
    color: #434;
}

#dept_wise {
    color: blue;
    margin-left: 40%;
    display: block;
    border: 2px solid cyan;
    border-radius: 6px;
    width: 200px;
}

#dept_wise:hover {
  color: green;
  background-color: blue;
}

#goback {
    margin-left: 50%;
}

</style>

</head>
<body>
<h3 class='tableHeader'>Teachers</h3>

<table class='teacher' border = 2>
         <tr>
            <td>Department</td>
            <td>TOTAL</td>
         </tr>
         @foreach ($teachers as $teacher)
         <tr>
            <td>{{ $teacher->department }}</td>
            <td>{{ $teacher->TOTAL }}</td>
         </tr>
         @endforeach
</table>

<a href="allUsers" id='goback'>Go Back</a>

</body>
</html>