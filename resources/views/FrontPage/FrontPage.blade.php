<?php
// set the expiration date to one hour ago
setcookie("user", "", time() - 3600);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Lab Manager</title>

	<!-- style -->
<style>
.Header {
    background-color: #212;
    font-weight: bolder;
    width: 100%;
    margin-bottom: 10px;
    margin-right: 10px;
}

#headline {
    font-size: 30px;
    margin: 20px auto 20px auto;
    text-align: center;
    color: rgba(255, 0, 0, 0.6);;
}
.login-container {
  padding: 10px 10px 10px 68%;
}

/* Style the input field inside the navbar */
.Header input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 12px;
  border: none;
  width: 150px; /* adjust as needed (as long as it doesn't break the topnav) */
}

.Header input[type=password] {
  padding: 6px;
  margin-top: 8px;
  font-size: 12px;
  border: none;
  width: 150px; /* adjust as needed (as long as it doesn't break the topnav) */
}

/* Style the button inside the input container */
.Header .login-container button {
  float: right;
  padding: 3px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.Header .login-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .login-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .login-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc; 
  }
}


h1 {
    font-family: fantasy;
    font-weight: bold;
    text-decoration: underline;
    color: black;
}

h1 a{
    color: black;
}

h1 a:hover {
    color: blueviolet;
    color: #cc8033;
    cursor: default;

}

.Content {
    overflow: auto;
    overflow-y: scroll;
}

.contentPara {
    font-weight: bold;
}

body {
    background-image: linear-gradient(to right, lightgrey,lightgrey,lightgrey,darkgrey,darkgrey,lightgrey,lightgrey);
}
/* All about right Navigation Bar included here */
.sideNav {
    float: left;
    overflow: auto;
    width: 25%;
    min-width: 20%;
    height: 450px;
    z-index: 100;
    background: #212;
    height: cover;
    margin-right: 10px;
}

.sideNav a {
    color: powderblue;
    padding: 20px;
    display: block;
    text-decoration: none;
    cursor: pointer;
}

.sideNav a:hover {
    background-color: rgba(170, 0, 0,0.3);
    color: white;
}

form div {
    margin-left: 75px;
}

	
</style>
	
</head>
<body>
	<?php
	    date_default_timezone_set("Asia/Dhaka");
		$title="Home";
		$loc = "			(Location: /FrontPage.php)";
	?>
<div class="Header">
	<div id="headline">Lab Equipment</div>
    <div class="login-container">
    <form method="POST" action="Users/myPage.php">
      <input type="text" placeholder="Email" name="email">
      <input type="password" placeholder="Password" name="password">
      <button type="submit">Login</button>
    </form>
  </div>
</div>
<div class="sideNav">
	<nav>
		<a href="/Users">Users</a>
		<a href="/Items">Manage Items</a>
		<a href="/Inventory">Manage Inventory</a>
		<a href="/Orders">Order Items</a>
	</nav>
</div>
<div class="Content">
	<h1><a href="#">About Project</a></h1>
	<div class="contentPara">
		Lab Equipment Management System is a browser based system that is designed to store, process, retrieve and analyze information concerned with the administrative and inventory management within laboratories of an institution. This project aims at maintaining all the information pertaining to users, different items available in a institution and help them manage in a better way.
	</div>
</div>
<?php $Today=date('Y-m-d h:i:sa');
echo "Time is ",$Today,'<br>','<br>';
$Today=date_create($Today);
$newFile = fopen('log.txt', 'a');
$time=$Today->format("d M, Y | h : i : sa");
$txt="\n";
$space=" | ";
fwrite($newFile, $time);
fwrite($newFile, $space);
fwrite($newFile, $title);
fwrite($newFile, $loc);
fwrite($newFile, $txt);
fclose($newFile);
?>
</body>
</html>